const fs = require("fs");
const path = require("path");

function rename(from, to, callback) {
  if (!from || !to) {
    throw new Error("filename was not provided");
  }

  let arr = from.split("/");
  let parentPath = arr.slice(0, arr.length - 1).join("");

  fs.rename(from, path.join(parentPath, to), (err) => {
    if (err) {
      callback(err);
    }
  });
}

module.exports = rename;
