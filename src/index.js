const rimraf = require("./rimraf");
const rename = require("./rename");

function main() {
  let args = process.argv;
  let func = args[2];

  if (func === "rimraf") {
    let index = args.indexOf("--path") + 1;
    if (index === 0) {
      throw new Error("-path flag was not provided");
    }

    rimraf(args[index], (err) => {
      if (err) {
        throw err;
      }
    });
  } else if (func === "rename") {
    let fromIndex = args.indexOf("--from") + 1;
    let toIndex = args.indexOf("--to") + 1;

    if (fromIndex === 0 || toIndex === 0) {
      throw new Error("A flag was not provided");
    }

    rename(args[fromIndex], args[toIndex], (err) => {
      if (err) {
        console.log(err);
      }
    });
  } else {
    throw new Error("Instruction was not provided");
  }
}

main();
