const fs = require("fs");

function rimraf(path, callback) {
  fs.rm(path, { recursive: true, force: true }, (err) => {
    if (err) {
      return callback(err);
    }
  });
}

module.exports = rimraf;
